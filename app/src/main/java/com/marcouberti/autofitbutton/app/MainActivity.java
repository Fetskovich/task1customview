package com.marcouberti.autofitbutton.app;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.marcouberti.autofitbutton.AutoFitButton;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    Button taskButton;
    EditText taskText;
    AutoFitButton fitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initUIComponents();

    }

    String currentEditText = "";
    int lastCharNormalPosition = 0;


    @Override
    public void onClick(View v) {
        String userText = taskText.getText().toString();
        Log.v("myLog", fitButton.getTextSize()+"<-CurrentSize, MinSize -> "+fitButton.getMinTextSize());
        if(fitButton.getTextSize()> fitButton.getMinTextSize()) {
            fitButton.setText(userText);
            lastCharNormalPosition = userText.length();
        }
        else{
            setButtonTextWithSmallInfo(userText);
        }
    }

    private void initUIComponents(){
        taskButton = (Button)findViewById(R.id.button_text_to_fitbutton);
        fitButton = (AutoFitButton)findViewById(R.id.button_autofit);
        taskText = (EditText)findViewById(R.id.edit_text);
        taskButton.setOnClickListener(this);
    }

    private void setButtonTextWithSmallInfo(String userText){
    String userConcatText = saveNormalTextInVariableAndReturnUserConcatText(userText);
        fitButton.setText(userConcatText);
    }

    private String saveNormalTextInVariableAndReturnUserConcatText(String userText){
        currentEditText = userText;
        String userConcatText = userText.substring(0, lastCharNormalPosition);
        userConcatText += "...";
        return userConcatText;
    }

}