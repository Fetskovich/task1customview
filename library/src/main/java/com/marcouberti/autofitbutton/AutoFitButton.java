package com.marcouberti.autofitbutton;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.Button;
import android.widget.TextView;

public class AutoFitButton extends Button {

    private CharSequence text = "";
    private float textSize, minTextSize;
    private float maxTextSize;
    private final String MY_LOGS = "myLog";

    public AutoFitButton(Context context) {
        super(context);
        init();
    }

    public AutoFitButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.AutoFitButton,
                0, 0);
        try {
            initSizes(a);
        } finally {
            a.recycle();
        }
        init();
    }

    private void initSizes(TypedArray a) {
        minTextSize = a.getDimensionPixelSize(R.styleable.AutoFitButton_minTextSize, -1);
        maxTextSize = a.getDimensionPixelSize(R.styleable.AutoFitButton_maxTextSize, 80);
    }

    private void init() {
        CharSequence cs = this.getText();
        if (cs != null) {
            this.text = this.getText();
            if (this.getTransformationMethod() != null) {
                this.text = this.getTransformationMethod().getTransformation(getText(), this);
            }
        }
        this.textSize = this.getTextSize();
    }


    @Override
    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        super.onTextChanged(text, start, lengthBefore, lengthAfter);
        this.text = text;
        this.textSize = this.getTextSize();
        if (lengthBefore != lengthAfter) {
            Log.d(MY_LOGS, textSize / getResources().getDisplayMetrics().density + " min text size: " + minTextSize / getResources().getDisplayMetrics().density);
            conditionWithSettingBestTextSize();
        }
    }


    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        int innerWidth = getInnerWidth();
        setBestTextSize(innerWidth);
    }


    private int getInnerWidth() {
        Drawable[] drawable = getCompoundDrawables();
        Drawable leftDrawable = drawable[0];
        Drawable rightDrawable = drawable[2];

        int ldw = 0, rdw = 0;
        if (leftDrawable != null) {
            ldw = leftDrawable.getMinimumWidth() + this.getCompoundDrawablePadding();
        }

        if (rightDrawable != null) {
            rdw = rightDrawable.getMinimumWidth() + this.getCompoundDrawablePadding();
        }

        int innerWidth = getMeasuredWidth() - getPaddingLeft() - getPaddingRight() - ldw - rdw;
        return innerWidth;
    }

    private void conditionWithSettingBestTextSize() {
        if (textSize > minTextSize) {
            setBestTextSize(getInnerWidth());
        } else Log.d(MY_LOGS, "Size doesn't change");
    }

    private void setBestTextSize(int innerWidth) {
        if (innerWidth <= 0) return;

        this.textSize = this.getTextSize();

        TextPaint paint = this.getPaint();
        StaticLayout sl;
        boolean found = false;
        while (!found && compareTextSizeWithMinSize()) {
            paint.setTextSize(this.textSize);
            sl = new StaticLayout(this.text, paint, innerWidth, Layout.Alignment.ALIGN_CENTER, 0, 0, true);
            if (sl.getLineCount() <= 1) {
                found = true;
            } else {
                this.textSize -= 1f;
            }
        }
        this.setTextSize(((this.textSize - 1f) / getResources().getDisplayMetrics().density));
    }

    private boolean compareTextSizeWithMinSize() {
        return this.textSize > 0 && this.textSize >= this.minTextSize;
    }


    public float getMinTextSize() {
        return minTextSize;
    }

    public float getMaxTextSize() {
        return maxTextSize;
    }
}
